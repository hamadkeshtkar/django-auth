from django.contrib.auth.models import User
from rest_framework import generics, permissions, serializers, views
from .serializer import AccountRegisterSerializer
from rest_framework import status
from django.core.mail import EmailMessage
from .token import account_activation_token
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.http import JsonResponse

class UserList(generics.ListCreateAPIView):
    permission_classes = [permissions.IsAuthenticated]
    queryset = User.objects.all()
    serializer_class = AccountRegisterSerializer
    
class UserCreateAPI(views.APIView):
    def post(self, request, format=None):
        user = AccountRegisterSerializer(data=request.data)
        if user.is_valid():
            user = user.save(is_active=False)
            email = EmailMessage(
                "Email Activition Link",
                'http://localhost:8000/api/auth/activate/' +
                urlsafe_base64_encode(force_bytes(user.pk)) + '/' +
                account_activation_token.make_token(user),
                to=[user.email]
            )
            email.send()
            return JsonResponse({'message': 'Please confirm your email address to complete the registration'}, status=status.HTTP_201_CREATED)
        return JsonResponse(user.errors, status=status.HTTP_400_BAD_REQUEST)

def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        return JsonResponse({'message': 'Thank you for your email confirmation. Now you can login your account.'}, status=status.HTTP_201_CREATED, safe=False)