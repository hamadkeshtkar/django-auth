from django.urls import path, include
from .views import *
# Setup the URLs and include login URLs for the browsable API.
urlpatterns = [
    path('users/', UserList.as_view()),
    path('auth2/', include('oauth2_provider.urls', namespace='oauth2_provider')),
    path('social/', include('rest_framework_social_oauth2.urls')),
    path('auth/register/', UserCreateAPI.as_view()),
    path('auth/activate/<slug:uidb64>/<slug:token>/', activate),
    path('auth/password_reset/', include('django_rest_passwordreset.urls', namespace='password_reset')),
]