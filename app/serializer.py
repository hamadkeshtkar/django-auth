from rest_framework import serializers
from django.contrib.auth import get_user_model

User = get_user_model()

class AccountRegisterSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(max_length=250, help_text="REQUIRED")
    
    class Meta:
        model = User
        fields = ['username','email','password']
        
    def create(self, validated_data):
        user = User(
            email=validated_data['email'],
            username=validated_data['username']
        )
        user.set_password(validated_data['password'])
        user.save()
        return user